package org.piglatin;

import org.junit.Assert;
import org.junit.Test;


public class PiglatinTest {

    /*
     * Words that start with a consonant have their first letter moved to the end of the word and the letters “ay" added to the end.
     * Hello becomes Ellohay
     */
    @Test
    public void startingWithConsonant() {
        testTranslation("Ellohay", "Hello");
        testTranslation("ccloudmay", "mccloud");
        testTranslation("histay", "this");
        testTranslation("hingtay", "thing");
    }

    /*
     * Words that start with a vowel have the letters "way" added to the end.
     * apple becomes appleway
     */
    @Test
    public void startingWithVowel() {
        testTranslation("appleway", "apple");
        testTranslation("apPLEway", "apPLE");
    }

    /*
     * Words that end in "way" are not modified. "stairway" stays as "stairway"
     */
    @Test
    public void endingWithWay() {
        testTranslation("starway", "starway");
        testTranslation("staRWay", "staRWay");
    }

    /*
     * Punctuation must remain in the same relative place from the end of the word.
     * "can’t" becomes "antca’y"
     * "end." becomes "endway."
     */
    @Test
    public void punctuationApostrophe() {
        testTranslation("antca'y", "can't");
        testTranslation("antca’y", "can’t");
    }

    @Test
    public void punctuationDot() {
        testTranslation("endway.", "end.");
        testTranslation("tartsay.", "start.");
    }

    /*
     * Hyphens are treated as two words. "this-thing" becomes "histay-hingtay"
     */
    @Test
    public void hyphens() {
        testTranslation("histay-hingtay", "this-thing");
    }

    /*
     * Capitalization must remain in the same place.
     * Beach becomes Eachbay
     * McCloud becomes CcLoudmay
     */
    @Test
    public void capitalizationAtBeginning() {
        testTranslation("Eachbay", "Beach");
    }

    @Test
    public void capitalizationInTheMiddle() {
        testTranslation("CcLoudmay", "McCloud");
    }

    private void testTranslation(String expected, String input) {
        Assert.assertEquals(expected, Translator.translate(input));
    }
}
