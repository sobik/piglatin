package org.piglatin;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Translator {

    private static final String VOWELS = "aeiouy";
    private static final String WAY = "way";
    private static final String AY = "ay";
    private static final String SEPARATORS = ",\\.!?\\s-";
    private static final Pattern paragraphGroupPattern = Pattern.compile(String.format("([%1$s]+|[^%1$s]+)", SEPARATORS));
    private static final Pattern separatorPattern = Pattern.compile(String.format("[%s]+", SEPARATORS));
    private static final String PUNCTUATIONS = "'’";
    private static final String punctuationsPattern = String.format("[%s]", PUNCTUATIONS);

    public static String translate(String input) {
        final Matcher matcher = paragraphGroupPattern.matcher(input);
        final StringBuilder sb = new StringBuilder();
        while (matcher.find()) {
            if (isSeparator(matcher.group())) {
                sb.append(matcher.group());
            } else {
                sb.append(translateWord(matcher.group()));
            }
        }
        return sb.toString();
    }

    private static String translateWord(String orig) {
        final String input = stripPunctuation(orig);
        final String transformed = applyCase(transformSyllables(input), input);
        return applyPunctuation(transformed, orig);
    }

    private static String transformSyllables(String input) {
        if (input.toLowerCase().endsWith(WAY)) {
            return input;
        } else if (startsWithVowel(input)) {
            return input + WAY;
        } else {
            return (input.substring(1) + input.charAt(0)) + AY;
        }
    }

    private static String applyCase(String input, String original) {
        String lettersOnly = stripPunctuation(original);
        List<Integer> upperCasePositions = new ArrayList<>();
        for (int i = 0; i < lettersOnly.length(); i++) {
            if (isUpperCaseAt(lettersOnly, i)) {
                upperCasePositions.add(i);
            }
        }
        StringBuilder result = new StringBuilder(input.toLowerCase());
        upperCasePositions.forEach(position -> result.setCharAt(position, Character.toUpperCase(result.charAt(position))));
        return result.toString();
    }

    private static String applyPunctuation(String input, String original) {

        SortedMap<Integer, Character> punctuationPositions = new TreeMap<>();

        for (int i = 0; i < original.length(); i++) {
            int charPosition = original.length() - i - 1;
            if (isPunctuationAt(original, charPosition)) {
                punctuationPositions.put(i, original.charAt(charPosition));
            }
        }

        StringBuilder result = new StringBuilder(input);
        punctuationPositions.keySet().forEach((position) -> result.insert(result.length() - position, punctuationPositions.get(position)));

        return result.toString();
    }

    private static boolean isPunctuationAt(String original, int i) {
        return PUNCTUATIONS.indexOf(original.charAt(i)) > -1;
    }

    private static boolean isUpperCaseAt(String text, int i) {
        final char letter = text.charAt(i);
        return !isPunctuationAt(text, i) && letter == Character.toUpperCase(letter);
    }

    private static boolean startsWithVowel(String input) {
        return VOWELS.indexOf(input.toLowerCase().charAt(0)) > -1;
    }

    private static String stripPunctuation(String input) {
        return input.replaceAll(punctuationsPattern, "");
    }

    private static boolean isSeparator(String text) {
        return separatorPattern.matcher(text).matches();
    }
}
